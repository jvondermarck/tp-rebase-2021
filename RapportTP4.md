# TP4 - GitLab

## Démarrage

- `git remote add personal git@gitlab.com:jvondermarck/tp-rebase-2021.git`

## Votre branche

- `git pull personal main` : pour récupérer la branche que je viens de créer sur Gitlab
- `git add README.md` --> `git commit -m "feat(README): added a line #1"` --> `git push origin 1- customize-readme` > Il est impossible de push sur le origin car c’est celui d’un repo extérieur dont on n’a pas les droits.
- `git push personal 1-customize-readme` --> ca marche car je suis dans mon remote à moi
- 'git pull personal main' --> pour mettre a jour mon main en local

## Merge Request

- Que constatez-vous à propos de vos branches ? De votre issue ?
  > Notre issue est maintenant close et notre branche a disparu également.

## Rapport

- `git pull personal 2-ajout-rapport`
- `git add Rapport.md` --> `git commit -m "feat(Rapport): added a rapport #2"` --> `git push origin 2-ajout-rapport`
