# TP6 - Commandes avancés

## Indexation avancée
- `git commit -m "feat(): add styles and edit readme"`
- `git diff`
- `sed -i 's/regular-text/regular-regular-text/g' *`

- `git restore --patch`

  ![](Screenshots/restore1.jpg)
  ![](Screenshots/restore2.jpg)
  ![](Screenshots/restore3.jpg)

## Git Log

- `git clone git@github.com:hakimel/reveal.js.git`

- `git log --help`
  > %h : abbreviated commit hash
  > %an : author name
  > --graph : le graphe
  > %cr : committer date, relative

- `git log --graph --pretty=format:"%h %an %cr"`
![](Screenshots/gitlog.jpg)

# Git comme base de données

Donnez les commandes permettant de lister :

1. les commits introduits dans la branche master qui ne font pas partie du tag 4.4.0

- `git log master --not 4.4.0`
- `git log 4.4.0..master`

2. les commits de merge appartenant à 4.3.1 mais pas 4.3.0

- `git log --merges 4.3.1 --not 4.3.0`

3. les commits non issus d'un merge dont le message contient le mot "typo"

- `git log --no-merges --grep="typo"`

Au total, combien de personnes ont participé au dépôt (nombre de committeurs) ? Donnez la commande git log qui vous a aidé et expliquez votre manière de procéder.

- `git log --pretty="%an" | sort | uniq | wc -l`

> Cette commande affiche le nombre de commits de chaque personne par ordre alphabétique, et avec la pipe `wc -l`, cela compte juste le nombre de lignes de la commande `git shortlog -s` qui est égale à 336 contribueurs. 

Établissez le classement (top 10) des contributeurs les plus prolifiques. Vous ne considérerez que la branche master et les commits non issus d'un merge. Comme avant, vous donnerez la commande git log utilisée et le cheminement de votre calcul.

- `git log --pretty="%an" | sort | uniq -c | sort -nr | head -n 10`

```
  2154 Hakim El Hattab
  94 hakimel
  29 VonC
  28 Michael Kühnel
  27 Benjamin Tan
  16 David Banham
  16 Chris Lawrence
  13 Owen Versteeg
  13 Adam Spiers
  12 Rory Hardy
```

## Questions
1. Quelle commande permet de supprimer les quatre derniers commits de la branche ?

  - `git reset --short HEAD^4`

2. Quelle est la différence entre une stash et un patch ? Quelle commande utiliseriez-vous pour produire un patch à partir d'une stash ?

  - Un stash est un stockage en attente qui est temporaire, il permet de sauvegarder des fichiers et on peut les réapliquer plus tard
  - Un patch permet quand à lui de faire un fichier en ajoutant ou supprimant une ligne des modifications éffectués, et il a en plus du code, des métadonnées sur les commits.
  - Créer un patch à partir d'un stash avec la commande suivante : `git stash show -p stash@{i} > stash-i.patch`

3. Quelle différence existe-t-il entre git reset --soft et --mixed ? Dans quel(s) cas utiliseriez-vous un mode plutôt que l'autre ?

  - La différence de `git reset --soft` et `git reset --mixed` sont que 
    - `--soft` : n'index pas la modification
    - `--mixed` : index la modification 

4. Dans quelles conditions est-il possible de modifier un commit via la commande git commit ?

  - Il est possible de modifier un commit avec la commande `git commit --amend` que si tout est en local (qu'on a rien push sur le serveur git).

5. Après un rebase, la synchronisation avec le dépôt remote échoue. Expliquez pourquoi et donnez la bonne commande à exécuter.

  - Si la référence distance n'est pas un ancêtre de la référence locale utilisée pour la réécrire. Il faut utiliser un `git push --force remote`. 

6. Quelle commande permet d'afficher les statistiques de modification de l'index ?

  - On peut utiliser la commande `git diff --stat` ou on peut utiliser la commande `git reflog`

<br>
@Julien Von Der Marck